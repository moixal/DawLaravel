@extends('templates.template')

@section('main-content')
<h2>Informació de la pel·lícula</h2>
<div>
    Identificador: {{ $movie->ID }} <br />
    Títol: {{ $movie->TITULO }} <br />
    Any: {{ $movie->ANYO }} <br />
    Puntació: {{ $movie->PUNTUACION }} <br />
    Votacions: {{ $movie->VOTOS }} <br />
</div>
@endsection