@extends('templates.template')

@section('main-content')
    <h2>Llistat</h2>
    <table>
    @foreach($list as $movie)
        <tr><td>{{ $movie->TITULO }} </td><td><a href="{{ route('movie', $movie->ID) }}">Mostra</a> </td></tr>
    @endforeach
    </table>
@endsection