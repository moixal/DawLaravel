<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/movie/{param}', [MovieController::class, 'getMovies'])->name('movie');

Route::get('/', function () {
    return view('home');
});

// Route::get('/list', [MovieController::class, 'listMovies'])->name('list');

Route::get('/list', function(){
    $list=App\Models\Movie::all();
    foreach($list as $movie) {
        echo $movie->TITULO . "<br  />";
    }
});

Route::get('/contact', function () {
    return view('contact');
});

