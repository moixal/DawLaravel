<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MovieController extends Controller
{
    function getMovies($param) {
        $rs = \DB::table('peliculas')->where('ID',$param)->first();
        return view('movie', array('movie'=>$rs));
    }
    function listMovies() {
        $rs = \DB::table('peliculas')->where('ID','<',30)->get();
        return view('list', array('list'=>$rs));
    }
}
