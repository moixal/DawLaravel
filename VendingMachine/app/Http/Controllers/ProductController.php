<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    function listProducts(Request $request) {
        $rs = \App\Models\Product::all();
        return view('products', array('products'=>$rs, 'username'=> $request->username));
    }
    
    function getProduct($product) {
        $rs = \App\Models\Product::where('id','=',$product)->get();
        return view('product', array('product'=>$rs));
    }
}
