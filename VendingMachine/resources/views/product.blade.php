<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <title>Vending JJJ S.A.</title>

        <script>
            console.log('Welcome Page');
        </script>
        <style>
            .product-text {
                display: flex;
                flex-direction: column;
                justify-content: flex-start;
                border: 3px solid black;
                width: 500px;
            }
            .product-con {
                display: flex;
                flex-direction: row;
                justify-content: space-evenly;
                border: 3px solid black;
                width: 1000px;
            }
            .product-con > img{
                width: 50%;
                border: 3px solid black;
            }
            body {
                font-family: 'Nunito', sans-serif;
                margin: 64px;
            }
            button {
                height: 32px;
            }
        </style>
    </head>
    <body>
        <header>
            <a href="{{ route('product-list') }}"><img src="https://www.downloadclipart.net/thumb/64553-white-arrow-with-red-background-left-icon.png" width="32" height="32" /></a>
        </header>

        <div class="product-con">
            <img src="{{ $product[0]->prod_img }}" alt="Product Image">
            <div class="product-text">
                <h4>{{ $product[0]->prod_name }}</h4>
                <h3>Preu: {{ $product[0]->prod_price }}</h3>
                <h3>País d'orígen: {{ $product[0]->prod_from }}</h3>
                <h3>Caloríes: {{ $product[0]->prod_kcal }}</h3>
                <h3>Info x100g: Greix: {{ $product[0]->prod_fat }}, Hidrats: {{ $product[0]->prod_carbohydrates }}, Proteínes: {{ $product[0]->prod_proteins }}</h3>
                <button><a href="{{ route('checkout') }}">Comprar</a></button>
            </div>
        </div>
    </body>
</html>
