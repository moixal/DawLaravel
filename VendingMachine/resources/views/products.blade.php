<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <title>Vending JJJ S.A.</title>

        <script>
            console.log( {{ $products }} );
        </script>
        <style>
            .grid-container {
                display: grid;
                column-gap: 10px;
                row-gap: 10px;
                grid-template-columns: auto auto auto;
            }
            .product {
                display: flex;
                flex-direction: column;
                justify-content: flex-start;
                border: 3px solid black;
                width: 250px;
            }
            .product > a > img{
                width: 100%;
            }
            body {
                font-family: 'Nunito', sans-serif;
                margin: 64px;
            }
        </style>
    </head>
    <body>
        <header>
            <a href="{{ route('login') }}"><img src="https://upload.wikimedia.org/wikipedia/commons/7/71/Human-gnome-logout.svg" width="32" height="32" /></a>

            <h2>Benvingut {{ auth()->user()->name }} tria el producte, el teu saldo és: {{ auth()->user()->saldo }}</h2>
        </header>
        <div class="grid-container">
            @foreach($products as $product)
            <div class="product">
                <a href="{{ route('product-detail', $product->id) }}">
                <img src="{{ $product->prod_img }}" alt="Product Image">
                <h4>{{ $product->prod_name }}</h4>
                <h3>Preu: {{ $product->prod_price }}</h3>
                </a>
            </div>
            @endforeach
        </div>

    </body>
</html>
