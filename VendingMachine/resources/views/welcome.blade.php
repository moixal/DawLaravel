<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Vending JJJ S.A.</title>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                margin: 64px;
            }
        </style>
    </head>
    <body class="antialiased">
        <h1>Benvingut a Vending JJJ S.A.</h1><br>
        <h4>Entra per a comprar..</h4>
        <a href="{{ route('login') }}">Anar al Login</a>
    </body>
</html>
