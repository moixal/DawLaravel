<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>

        <title>Vending JJJ S.A.</title>

        <script>
            console.log( <?php echo e($products); ?> );
        </script>
        <style>
            .grid-container {
                display: grid;
                column-gap: 10px;
                row-gap: 10px;
                grid-template-columns: auto auto auto;
            }
            .product {
                display: flex;
                flex-direction: column;
                justify-content: flex-start;
                border: 3px solid black;
                width: 250px;
            }
            .product > a > img{
                width: 100%;
            }
            body {
                font-family: 'Nunito', sans-serif;
                margin: 64px;
            }
        </style>
    </head>
    <body>
        <header>
            <a href="<?php echo e(route('login')); ?>"><img src="https://upload.wikimedia.org/wikipedia/commons/7/71/Human-gnome-logout.svg" width="32" height="32" /></a>

            <h2>Benvingut <?php echo e(auth()->user()->name); ?> tria el producte, el teu saldo és: <?php echo e(auth()->user()->saldo); ?></h2>
        </header>
        <div class="grid-container">
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="product">
                <a href="<?php echo e(route('product-detail', $product->id)); ?>">
                <img src="<?php echo e($product->prod_img); ?>" alt="Product Image">
                <h4><?php echo e($product->prod_name); ?></h4>
                <h3>Preu: <?php echo e($product->prod_price); ?></h3>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

    </body>
</html>
<?php /**PATH /home/john/Documents/jda/m07/DawLaravel/VendingMachine/resources/views/products.blade.php ENDPATH**/ ?>