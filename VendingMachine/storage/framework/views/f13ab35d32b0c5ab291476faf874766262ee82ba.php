<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Vending JJJ S.A.</title>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                margin: 64px;
            }
        </style>
    </head>
    <body class="antialiased">
        <a href="<?php echo e(route('welcome')); ?>"><img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.iconsdb.com%2Ficons%2Fdownload%2Fred%2Fhouse-512.png&f=1&nofb=1" width="32" height="32" /></a>

        <h1>Login</h1>

        <form method="POST">
            <?php echo csrf_field(); ?>
            <input name="name" type="name" placeholder="Nom.."><br>
            <input name="password" type="password" placeholder="Contrasenya.."><br>
            <button type="submit">Entra</button>
        </form>
    </body>
</html>
<?php /**PATH /home/john/Documents/jda/m07/DawLaravel/VendingMachine/resources/views/login.blade.php ENDPATH**/ ?>