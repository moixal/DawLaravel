<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <script>
            console.log( <?php echo e($products); ?> );
        </script>
        <style>
            .product {
                display: flex;
                flex-direction: column;
                justify-content: flex-start;
                border: 3px solid black;
                width: 250px;
            }
            .product > img{
                width: 100%;
            }
        </style>
    </head>
    <body>
        <header>
            <h2 style="display: flex; flex-direction: row;"><a href="<?php echo e(route('login')); ?>" style="transform: rotate(-180deg);">&#10151;</a> &nbsp; &nbsp; <a href="#">&#10151;</a></h2>
            <h2>Benvingut <?php echo e(auth()->user()->email); ?> tria el producte, el teu saldo és: <?php echo e(auth()->user()->saldo); ?></h2>
        </header>
         <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="product">
                <img src="<?php echo e($product->prod_img); ?>" alt="Product Image">
                <h4><?php echo e($product->prod_name); ?></h4>
                <h3>Preu: <?php echo e($product->prod_price); ?></h3>
                <button><a href="<?php echo e(route('product-detail', $product->id)); ?>">Mostra</a></button>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </body>
</html>
<?php /**PATH /home/john/Documents/jda/m07/DawLaravel/Vending2/vendingmachine2/resources/views/products.blade.php ENDPATH**/ ?>