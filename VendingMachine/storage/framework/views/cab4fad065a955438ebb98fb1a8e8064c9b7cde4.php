<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <script>
            console.log('Welcome Page');
        </script>
        <style>
            .product {
                display: flex;
                flex-direction: column;
                justify-content: flex-start;
                border: 3px solid black;
                width: 750px;
            }
            .product > img{
                width: 100%;
            }
        </style>
    </head>
    <body>
        <header>
            <h2 style="display: flex; flex-direction: row;"><a href="<?php echo e(route('product-list')); ?>" style="transform: rotate(-180deg);">&#10151;</a> &nbsp; &nbsp; <a href="#">&#10151;</a></h2>
        </header>

        <div class="product">
            <img src="<?php echo e($product[0]->prod_img); ?>" alt="Product Image">
            <h4><?php echo e($product[0]->prod_name); ?></h4>
            <h3>Preu: <?php echo e($product[0]->prod_price); ?></h3>
            <h3>País d'orígen: <?php echo e($product[0]->prod_from); ?></h3>
            <h3>Caloríes: <?php echo e($product[0]->prod_kcal); ?></h3>
            <h3>Info x100g: Greix: <?php echo e($product[0]->prod_fat); ?>, Hidrats: <?php echo e($product[0]->prod_carbohydrates); ?>, Proteínes: <?php echo e($product[0]->prod_proteins); ?></h3>
            <button><a href="<?php echo e(route('checkout')); ?>">Comprar</a></button>
        </div>
    </body>
</html>
<?php /**PATH /home/john/Documents/jda/m07/DawLaravel/Vending2/vendingmachine2/resources/views/product.blade.php ENDPATH**/ ?>