<?php

use Illuminate\Support\Facades\Route;use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::view('login', 'login');

Route::post('login', function(){
   $credentials =  request()->only("name", "password");

   Auth::attempt($credentials);
   if(Auth::attempt($credentials)){
       request()->session()->regenerate();
        return redirect('products');
   }
   return view("login");
})->name('login');

// Route::view('products', 'products');

Route::get('/products', [ProductController::class, 'listProducts'])->name('product-list');

Route::get('/checkout', function () {
    return view('checkout');
})->name('checkout');

Route::get('/products/{product}', [ProductController::class, 'getProduct'])->name('product-detail');
