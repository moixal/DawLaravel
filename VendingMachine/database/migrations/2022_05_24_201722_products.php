<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('prod_name');
            $table->string('prod_img');
            $table->float('prod_price')->nullable();
            $table->string('prod_from');
            $table->integer('prod_kcal')->nullable();
            $table->integer('prod_100g')->nullable();
            $table->float('prod_fat')->nullable();
            $table->float('prod_carbohydrates')->nullable();
            $table->float('prod_proteins')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
