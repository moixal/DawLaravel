--  Inserts de dos usuaris (contrasenya -> 'password')

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `saldo`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jordi', 'jordi@jordi.com', '2022-05-24 18:30:51', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '50', 'ETnTH3Eqhr', '2022-05-24 18:30:51', '2022-05-24 18:30:51'),
(2, 'carles', 'carles@carles.com', '2022-05-24 18:31:04', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '22', '76ptBsMCCT', '2022-05-24 18:31:04', '2022-05-24 18:31:04');


-- Inserts de productes

INSERT INTO `products` (`id`, `prod_name`, `prod_img`, `prod_price`, `prod_from`, `prod_kcal`, `prod_100g`, `prod_fat`, `prod_carbohydrates`, `prod_proteins`, `created_at`, `updated_at`) VALUES
(1, 'Patates Fregides', 'https://www.bestwaywholesale.co.uk/img/products/1000/1/5000328142081.jpg', 12.00, 'Espanya', 23, 12, 12.00, 12.00, 12.00, NULL, NULL),
(2, 'Xocolatina', 'https://cdn.shopify.com/s/files/1/0274/7315/products/snick_9b5cdcc7-43bd-4494-8556-adf857d799a5_1000x1000.jpg?v=1589978280', 12.00, 'EEUU', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(3, 'Aigua Viladrau', 'https://cdn.grupoelcorteingles.es/SGFM/dctm/MEDIA03/202003/02/00118630000356____3__600x600.jpg', 56.00, 'Espanya', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(4, 'Suc de Prèsec', 'https://media1.cestaclick.es/1121-thickbox_default/Juver-Nectar-de-Melocoton-Seleccion-200ml.jpg', 23.00, 'Portugal', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(5, 'Vodka', 'https://d2d8wwwkmhfcva.cloudfront.net/800x/d2lnr5mha7bycj.cloudfront.net/product-image/file/large_f9c5ab63-e2b3-43ef-8e86-65156405baef.jpg', 12.00, 'Russia', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(6, 'Galetes', 'https://www.almacenesjr.com/wp-content/uploads/300433.jpg', 21.00, 'Espanya', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(7, 'Cacaolat', 'https://jmposner.e2ecdn.co.uk/Products/CACAOLAT-ORIGINAL-001-web.jpg?w=1000&h=1000&quality=90&scale=canvas', 89.00, 'Espanya', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(8, 'Fanta', 'https://americanfizz.co.uk/image/cache/catalog/american-soda/fanta/fanta-orange-12oz-355ml-800x800.png', 12.00, 'Alemanya', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(9, 'Raviolis', 'https://a0.soysuper.com/ed5143a71725542a74736d49d7060a5e.340.340.0.min.wmark.d76161de.jpg', 456.00, 'Bèlgica', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(10, 'Donuts', 'https://cdn.grupoelcorteingles.es/SGFM/dctm/MEDIA03/201903/29/00120671600169____2__600x600.jpg', 62.00, 'França', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(11, 'Tomàquet', 'https://www.tuclubdecompras.es/contenidos/articulos/0/439-002-2-1.jpg', 112.00, 'Russia', 12, 12, 12.00, 12.00, 12.00, NULL, NULL),
(12, 'Vodka', 'https://a1.soysuper.com/71c3c7a0b1c3c7ed29a67582f043b168.600.0.0.0.wmark.2d4451ce.jpg', 223.00, 'Mèxic', 12, 12, 12.00, 12.00, 12.00, NULL, NULL);





