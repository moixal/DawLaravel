# Vending Machine Managament fet en php Laravel.

### Per Joaquim Cabello, Jan Maroto i Jose A. Armengod.

### [Enllaç del repositori.](https://codeberg.org/moixal/DawLaravel)

## Com executar el projecte.
1. Configurem el fitxer '.env', per a que apunti a una base de dades vuida, amb un usuari amb permís de lectura i escriptura. 
![](https://pad.nixnet.services/uploads/51d872d0-8574-4563-a5b3-78bc213022bd.png)

2. Fem la migració de la base de dades amb: ```php artisan migrate```
![](https://pad.nixnet.services/uploads/f93a3e42-7a8f-486c-ad11-f2d290dd363b.png)

3. Entrem a MySQL / MariaDB i executem el fitxer 'database/database.sql' amb la comanda: ```source ruta/del/fitxer.sql```, dintre de la bdd del projecte per a introduïr usuaris i productes.
![](https://pad.nixnet.services/uploads/f0394a0f-528f-4b4a-b634-c6d1d10f2382.png)

4. Servim l'aplicaió amb la comanda: ```php artisan serve```.
![](https://pad.nixnet.services/uploads/761a0f9f-a3ba-4ae6-8996-e7f8c168971c.png)

5. Provem el resultat. Els usuaris creats de moment són: 'jodi' i 'carles' i la contresenyes en abdos casos és 'password'.
![](https://pad.nixnet.services/uploads/80aa64df-7f05-47af-862e-033031fe1f3e.png)
![](https://pad.nixnet.services/uploads/9cf4ca6c-ba06-4c38-93bd-4c08f3ae480c.png)
![](https://pad.nixnet.services/uploads/a684ffbe-e842-46e4-92c8-68143ac4ab9d.png)

Això és tot.